/* Najważniejsza funkcja w tym skrypcie to sortStepByStep.
Porządkuje ona tablicę liczb i pokazuje w konsoli krok po kroku kolejne etapy sortowania tablicy.
Wydajniejszą implementację sortowania szybkiego umieściłem w pliku quicksort.js */

var quicksort = function (array) {
	if (array.length < 2) {
		return array
	} else {

		var pivot = array[0]
		var lesserValues = [];
		var greaterValues = [];

		for (let i = 1; i < array.length; i++) {
			if (array[i] < array[0]) {
				lesserValues.push(array[i]);
			} else {
				greaterValues.push(array[i]);
			}

		}

		return [lesserValues, [pivot], greaterValues];

	}

}





var sortArrayOfArrays = function (arrayOfArrays) {

	let newArray = [];


	for (let i = 0; i < arrayOfArrays.length; i++) {

		if (arrayOfArrays[i].length < 2) {
			newArray.push(arrayOfArrays[i])

		} else {
			let arraysToBeAdded = quicksort(arrayOfArrays[i])
			newArray.push(arraysToBeAdded[0]);
			newArray.push(arraysToBeAdded[1]);
			newArray.push(arraysToBeAdded[2]);
		}





	}

	return newArray;

}



checkType = function (arg) {

	if (!Array.isArray(arg)) {
		console.log("Błędny format");
		return false;
	}
	let j = 0;
	let k = 0;
	for (let i = 0; i < arg.length; i++) {
		if (typeof(arg[i]) === "number") {
			j++;
		} else {
			if (Array.isArray(arg[i])) {
				let l = 0;
				for (let m = 0; m < arg[i].length; m++) {
					if (typeof(arg[i][m]) === "number") {l++;}
				}
				if (l === arg[i].length) {
					k++;
				}
			}
		}
	}

	if ( j === arg.length) {
		return [arg];
	} else	if ( k === arg.length) {
		return arg;
	} else {
		return false;
	}
	

}


var transformArray = function(arg) {
	var newArray = [];
	for (let i = 0; i < arg.length; i++) {

		if (arg[i].length === 0) {

		} else if (arg[i].length === 1) {
			newArray.push(arg[i][0]);
		} else if (arg[i].length > 1) {

			for (let j = 0; j < arg[i].length; j++) {
				newArray.push(arg[i][j]);
			}


		}



	}

	return newArray;

}

var isOrdered = function (array) {
	for (let i = 1; i < array.length; i++) {
		if (array[i] < array[i - 1]) {
			return false
		}
	}
	return true
}





var sortStepByStep = function (arg) {

	arg = checkType(arg)


	if (!arg) {
		console.log("Błędny format");
		return false;
	} 


	while (!isOrdered(transformArray(arg))) {
		arg = sortArrayOfArrays(arg)
		console.log(arg)
	}

	return transformArray(arg)
}