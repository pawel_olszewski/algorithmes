// Silnia

var factorial = function (x) {

	if (x !== parseInt(x) || x < 0) {
		return "Błędny format";
	} else if (x === 0 || x === 1) {
		return 1;
	} else {
		return x * factorial (x-1);
	}


};

// Ciąg Fibonacciego

var fibonacci = function (x) {

	if (x !== parseInt(x) || x < 0) {
		return "Błędny format";
	} else if (x === 0) {
		return 0;
	} else if (x === 1) {
		return 1;
	} else {
		return fibonacci(x-1) + fibonacci(x-2);
	}


};