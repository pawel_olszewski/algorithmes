addNode = document.getElementById("addNode");
tbody = document.getElementById("tbody");
checkPath = document.getElementById("checkPath");
first = document.getElementById("first");
second = document.getElementById("second");
result = document.getElementById("result");
helpText = document.getElementById("helpText");
helpButton = document.getElementById("helpButton");





var remove = function (e) {
	tbody.removeChild(e.target.parentNode.parentNode);
};

var firstX = document.getElementById("firstX");
firstX.addEventListener("click", remove, true);






addNode.addEventListener("click", function () {

	var newNodeInput = document.createElement("input");
	var newNodeTd = document.createElement("td");
	var newEdgesInput = document.createElement("input");
	var newEdgesTd = document.createElement("td");
	var newRemoveText = document.createTextNode("X");
	var newRemoveButton = document.createElement("button");
	var newRemoveTd = document.createElement("td");

	var newTr = document.createElement("tr");

	newNodeInput.setAttribute("type", "text");
	newEdgesInput.setAttribute("type", "text");
	newNodeInput.className = "node";
	newEdgesInput.className = "edges";
	newRemoveButton.className = "remove";


	newRemoveButton.appendChild(newRemoveText);
	newNodeTd.appendChild(newNodeInput);
	newEdgesTd.appendChild(newEdgesInput);
	newRemoveTd.appendChild(newRemoveButton);
	newTr.appendChild(newNodeTd);
	newTr.appendChild(newEdgesTd);
	newTr.appendChild(newRemoveTd);
	tbody.appendChild(newTr);

	newRemoveButton.addEventListener("click", remove, true);

}, true);






var graph = {};

checkPath.addEventListener("click", function () {

//żeby nie było pustych początku/końca
if (first.value === "" || second.value === "") {
	result.innerHTML = "Podaj dane ścieżki";
	return
}

graph = {};
var nodes = document.getElementsByClassName("node")
var edges = document.getElementsByClassName("edges")
var checked = [];

for (let i = 0; i < nodes.length; i++) {
	var arrayOfEdges = edges[i].value.split(",");
	for(let j = 0; j < arrayOfEdges.length; j++) {
		arrayOfEdges[j] = arrayOfEdges[j].trim()
	}
	graph[nodes[i].value] = arrayOfEdges;
}


var checklist = graph[first.value];
while (checklist !== undefined && checklist.length > 0) {
	if (checklist[0] === second.value) {
		result.innerHTML = "Jest ścieżka!";
		return;
	}
	if (graph[checklist[0]] !== undefined && checked.indexOf(checklist[0]) === -1) {
		checklist = checklist.concat(graph[checklist[0]])
		checked.push(checklist[0])
	}
	checklist.shift();

}

result.innerHTML = "Nie ma ścieżki :(";

}, true);


var showHelp = false
helpButton.addEventListener("click", function () {
	if (!showHelp) {
		helpText.innerHTML = 'W pierwszej tabelce podaj w lewej kolumnie węzły/wierzchołki grafu. Dla każdego wierzchołka podaj w prawej kolumnie listę wierzchołków, z którymi pierwszy ma być połączony. Oddziel znakiem przecinka , kolejne wierzchołki. Graniczące z przecinkami ciągi spacji (w tym pojedyncze spacje) są pomijane. <br /> W drugiej tabli podaj nazwy dwóch wierzchołków, a algorytm przeszukiwania wszerz poda Ci, czy istnieje ścieżka od pierwszego wierzchołka do drugiego.'
		helpButton.innerHTML = "Ukryj"
		showHelp = true;

	} else {
		helpText.innerHTML = ""
		helpButton.innerHTML = "Pomoc"
		showHelp = false;

	}

}, true)
