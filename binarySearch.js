	var border1 = 1;
	var border2 = 128;

	var getMid = function (a, b) {
		return Math.floor((a+b)/2);
	}

	var mid;

	alert("Pomyśl liczbę naturalną od 1 do 128 (włącznie), a ja spróbuję ją zgadnąć.")

	while (border1 !== border2) {
		let mid = getMid(border1, border2);
		if(confirm("Czy liczba jest większa od " + mid + "?")) {
			border1 = mid + 1;
		} else {
			border2 = mid;
		}
		console.log(border1 + "  " + border2)
	}

	alert("Ta liczba to " + border1 + "!");