var sort = function (array) {

	let length = array.length;
	let oldArray = array;
	let newArray = [];

	for (let i = 0; i < length; i++) {

		let minimum = oldArray[0];

		for (let j = 0; j < oldArray.length; j++) {
			if (minimum > oldArray[j]) {
				minimum = oldArray[j];
			}
		}

		let index = oldArray.indexOf(minimum);
		let oldArray1 = oldArray.splice(0, index);
		let oldArray2 = oldArray.splice(1);
		oldArray = oldArray1.concat(oldArray2);

		newArray.push(minimum)

	}

	return newArray;

};