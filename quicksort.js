checkType = function (arg) {

	if (!Array.isArray(arg)) {
		console.log("Błędny format");
		return false;
	}

	let j = 0;

	for (let i = 0; i < arg.length; i++) {
		if (typeof(arg[i]) === "number") {
			j++;
		} 
	}

	if (j === arg.length) {
		return true;
	} else {
		return false;
	}
	

}


var quicksort = function (array) {

	if (!checkType(array)) {
		console.log("Błędny format")
		return false

	}



	if (array.length < 2) {
		return array
	} else {

		var pivot = array[0]
		var lesserValues = [];
		var greaterValues = [];

		for (let i = 1; i < array.length; i++) {
			if (array[i] < array[0]) {
				lesserValues.push(array[i]);
			} else {
				greaterValues.push(array[i]);
			}

		}

		return quicksort(lesserValues).concat([pivot], quicksort(greaterValues))

	}

}