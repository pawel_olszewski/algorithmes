var euclideanAlgorithm = function (a, b) {
 
    if (a !== parseInt(a) || b !== parseInt(b)) {
        console.log("Błędny format");
        return;
    }
 
    var firstA = a;
    var firstB = b;
 
    var c, d;
 
    while (d !== 0) {

        var c = Math.floor(a / b);
        var d = a % b;
        console.log(a + " / " + b + " = " + c + " r. " + d);
        a = b;
        b = d;
 
    }
 
    console.log("NWD(" + firstA + "," + firstB + ") = " + a)
    return a;
 
}